import sys
# sys.path.append('../mbse')
sys.path.append('/home/honam/workspace/ode/pyur5/include/pyur5')
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

from utils import denormalize, normalize, get_data_jax
import jax.numpy as jnp 
from jax.lax import cond
from trajax.optimizers import CEMHyperparams, ILQRHyperparams, ilqr_with_cem_warmstart, cem, ilqr
import pickle
import math
import jax
from models.models import MLP

from gym.spaces import Box
import numpy as np
from gym.envs.classic_control.pendulum import angle_normalize
import pandas as pd

class EnsembleModel():
    def __init__(self, train_horizon=1, n_model=5, n_horizon=50, use_cos=False, action_max=1.0, task_typ='pole'):#, path_model, paths_params):
        # model_path = "checkpoints/revived-morning-99/best_model.pkl"
        # params_path = ["checkpoints/revived-morning-99/best_params.pkl",
        #             "checkpoints/jumping-cloud-101/best_params.pkl",
        #             "checkpoints/vital-morning-100/best_params.pkl",
        #             "checkpoints/confused-feather-96/best_params.pkl",
        #             "checkpoints/misunderstood-sponge-102/best_params.pkl"]
        
        # with open(model_path, "rb") as f:
        #     self.model = pickle.load(f)

        # self.params = []
        # for path in params_path:
        #     with open(path, "rb") as f:
        #         self.params.append(pickle.load(f))
        print("use_cos: ", use_cos)
        self.train_horizon = train_horizon
        self.n_model = n_model
        self.use_cos = use_cos
        
        self.target_state = jnp.array([math.pi, 0.0])
        self.cost_weights = jnp.array([10, 1, 0.1])
        
        self.action_max = action_max
        self.task_typ = task_typ
        # self.model_catalog = ModelSelection()
        # self.models, self.params = self.model_catalog.load_models(train_horizon=self.train_horizon,
        #                                                           n_model=self.n_model)
        if task_typ == "swingup":
            if self.action_max == 0.7:
                if self.use_cos:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_unique-sweep-20.pkl"
                else:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_devout-sweep-11.pkl"
            elif self.action_max == 1.0:
                if self.use_cos:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_pretty-sweep-5.pkl"
                else:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_spring-sweep-2.pkl"

            elif self.action_max == 0.5:
                if self.use_cos:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_firm-sweep-3.pkl"
                else:
                    model_path = "/home/honam/workspace/ode/pyur5/model/best_model_cerulean-sweep-2.pkl"
        elif task_typ == "pole":
            if self.use_cos:
                model_path = "/home/honam/workspace/ode/pyur5/model/best_model_skilled-sweep-28.pkl"
            # else:
            #     model_path = "/home/honam/workspace/ode/pyur5/model/best_model_spring-sweep-2.pkl"
                
        with open(model_path, "rb") as f:
            self.model = pickle.load(f)
        
        
        self.cem_params = CEMHyperparams(max_iter=200, sampling_smoothing=0.0, num_samples=200, evolution_smoothing=0.0,
                        elite_portion=0.1)  
        # TODO: test with different values of psd_delta 
        self.ilqr_params = ILQRHyperparams(maxiter=200, make_psd=True, psd_delta=1e-2)

        with open("/home/honam/workspace/ode/pyur5/metadata/metadata_{}_{}_{}.pkl".format(self.task_typ, self.action_max, self.use_cos), "rb") as f:
            self.metadata = pickle.load(f)
        
        if self.use_cos:
            self.x_dim = 5
        else:
            self.x_dim = 4
            
        self.u_dim = 1
        self.n_horizon = n_horizon
        
        # self.metadata['min_x'] = self.metadata['x'].min(axis=0)
        # self.metadata['max_x'] = self.metadata['x'].max(axis=0)

        self.obs_min = np.array(self.metadata['min_x'][:-1])
        self.obs_max = np.array(self.metadata['max_x'][:-1])
        
        self.action_min = np.array([self.metadata['min_x'][-1]])
        self.action_max = np.array([self.metadata['max_x'][-1]])
        
        print("obs_min", self.obs_min)
        print("obs_max", self.obs_max)
        
        print("action_min", self.action_min)
        print("action_max", self.action_max)
        
        self.obs_space = Box(low=self.obs_min, high=self.obs_max, dtype=np.float32)
        self.action_space = Box(low=self.action_min, high=self.action_max, dtype=np.float32)

    @staticmethod
    @jax.jit
    def normalize(x, u, metadata):
        inputs = jnp.concatenate([x, u]).transpose()
        inputs = normalize(inputs, metadata['mu_x'], metadata['std_x'])
        return inputs
    
    @staticmethod
    @jax.jit
    def denormalize(out, metadata):    
        out = denormalize(out, metadata['mu_y'], metadata['std_y'])
        return out
    
    def _predict(self, xu):
        y_pred = self.model.predict(xu)
        mu, sig = jnp.split(y_pred, 2, axis=-1)
        ds = mu.mean(0)
        return ds
    
    def normalize_and_predict(self, x, u):
        '''
            predict the dx given x and u after normalizing x and u
        '''
        # print("x before normalize: ", x)
        # print("u before normalize: ", u)
        xu = self.normalize(x, u, self.metadata)
        print("xu after normalize: ", xu)
        dx = self._predict(xu) #self.predict(xu)
        dx = self.denormalize(dx, self.metadata)

        #dx = self.denormalize(dx, self.metadata)
        print("dx value of shape: ", dx.shape)
        return dx
        
    # def predict(self, x, u=None):
    #     '''
    #         predict the dx given x and u
    #     '''
    #     if u is None:
    #         xu = x
    #     else:
    #         try:
    #             assert x.shape[1] == self.x_dim
    #             axis = 1
    #         except:
    #             axis = 0
        
    #         xu = jnp.concatenate([x, u], axis=axis)
    #     ds = jnp.array([self.models[i].apply(params, xu) for i, params in enumerate(self.params)]).mean(0)
    #     # print("ds before denormalize: ", ds)
    #     ds = self.denormalize(ds, self.metadata)
        
        # return ds
    
    def step(self, x, u):
        '''
            predict the next state given x and u
            
            input:
                x: (x_dim, )
                u: (u_dim, ) rescaled action
            
            output:
                x_next: (x_dim, )
                terminate: bool
                truncate: bool
                output_dict: dict
        '''
        # print("x before step: ", x)
        print("u before step: ", u)
        u = u * self.action_max # scale to the range of [-0.7, 0.7]
        dx = self.normalize_and_predict(x, u)
        x_next = x + dx
        if not self.use_cos:
            x_next = x_next.at[0].set(angle_normalize(x_next[0]))
        else:
            theta = jnp.arctan2(x_next[1], x_next[0])
            theta = angle_normalize(theta)
            x_next = x_next.at[0].set(jnp.cos(theta))
            x_next = x_next.at[1].set(jnp.sin(theta))

        print("x_next of shape: ", x_next.shape)
        # x_next = jnp.clip(x_next, self.obs_min, self.obs_max)
        reward = self._reward_fn(x, u)
        return x_next, reward, False, {}
    
    def _reward_fn(self, state, action, t=0):
        print("reward function called")
        
        # def _reward_non_cos(state, action, t):
        #     return -self._cost_fn(state, action, t)

        # def _return_cos(state, action, t):
        #     return -self._cost_fn(state, action, t)
        
        # return jax.lax.cond(self.use_cos, _return_cos, _reward_non_cos, state, action, t)
        
        
        return -self._cost_fn(state, action, t)
    
    def _cost_fn(self, state, action, t): 
        print("cost function called")
        print("state shape: ", state.shape)
        print("action shape: ", action.shape)
        print("state: ", state)
        print("action: ", action)
        assert state.shape == (self.x_dim,) and action.shape == (self.u_dim,)

        if self.use_cos:
            theta = jnp.arctan2(state[1], state[0]).reshape(1, )
            state = jnp.concatenate([theta, state[2:]])
            print("state shape: ", state.shape)
            print("theta: ", theta)

        theta = state[0]
        theta_dot = state[1]
        
        qs = self.cost_weights
        target_state = self.target_state
        
        theta_star = target_state[0]
        theta_dot_star = target_state[1]

        q_theta = qs[0]
        q_theta_dot = qs[1]
        q_u = qs[2]

        dtheta = theta - theta_star
        dtheta_dot = theta_dot - theta_dot_star
        dtheta = angle_normalize(dtheta)
        
        def running_cost(dtheta, dtheta_dot, action):
            return q_theta * jnp.sum(dtheta ** 2) + \
                + q_theta_dot * jnp.sum(dtheta_dot**2) \
                + q_u * jnp.sum(action ** 2)
        
        def terminal_cost(dtheta, dtheta_dot, action):
            return q_theta * jnp.sum(dtheta ** 2) + \
                + q_theta_dot * jnp.sum(dtheta_dot**2)

        return cond(t == self.n_horizon, terminal_cost, running_cost, dtheta, dtheta_dot, action)

    def _dynamics_fn(self, x, u, t):
        '''
            Constrained dynamics function
        '''
        print(x.shape)
        print(u.shape)
        print("x of dynamics_fn: ", x)
        print("u of dynamics_fn: ", u)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        # u = self.action_max * jnp.tanh(u) # hypertangent trick 
        # TODO: Make this flexible so that we pass in values for sim_dynamics
        x_next, _, _, _ = self.step(x, u)
        print("x_next of dynamics_fn: ", x_next)
        return x_next
    
    def _dynamics_fn_unconstrained(self, x, u, t):
        print(x.shape)
        print(u.shape)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        u = self.action_max * jnp.tanh(u) # hypertangent trick 
        x_next, _, _, _ = self.step(x, u)
        print("x_next of dynamics_fn: ", x_next)
        return x_next

    def _dynamics_fn_cond(self, constrained=True):
        if constrained:
            return self._dynamics_fn
        else:
            return self._dynamics_fn_unconstrained
    
    def forward_traj(self, x, n_horizon, optimizer='cem_ilqr', sac_policy=None):
        # x_traj = jnp.zeros((n_horizon, x.shape[0]))

        self.n_horizon = n_horizon
        rng = jax.random.PRNGKey(seed=0)
        actor_rng, rng = jax.random.split(rng, 2)
        
        u = jnp.zeros((n_horizon, self.u_dim))

        print(x.shape)
        print(u.shape)

        if optimizer == 'cem':
            out = cem(self._cost_fn, self._dynamics_fn, x, u, control_low=self.action_min,
                              control_high=self.action_max, hyperparams=self.cem_params)

        elif optimizer == 'sac':
            if sac_policy is None:
                with open("/home/honam/workspace/ode/pyur5/metadata/sac_policy_{}_{}_{}.pkl".format(self.train_horizon, self.n_model, self.n_horizon), "rb") as f:
                    sac_policy = pickle.load(f)

            #f = jax.vmap(sac_policy.get_action_for_eval, (None, 0, 0))
            us = []
            xs = []
            cost = 0
            for i in range(self.n_horizon):
                u = sac_policy.get_action_for_eval(x, rng, 0)
                u = sac_policy.dynamics_model_list[0].rescale_action(u) # scale actions
                us.append(u)
                xs.append(x)
                x, _, _, _ = self.step(x, u)
                
                cost_single = self._cost_fn(x, u, i)
                
                #_, cost_single = sac_policy.dynamics_model_list[0].evaluate(xs[-1], u, rescaled=True)
                cost += cost_single
            out = [jnp.array(xs), jnp.array(us), cost]
        else:
            if optimizer == 'cem_ilqr':
                out = ilqr_with_cem_warmstart(self._cost_fn, self._dynamics_fn_unconstrained, x, u, control_low=self.action_min,
                                control_high=self.action_max, ilqr_hyperparams=self.ilqr_params, cem_hyperparams=self.cem_params)
                xs = out[0]
                us = out[1]
                cost = out[2]
                
            elif optimizer == 'ilqr':
                out = ilqr(self._cost_fn, self._dynamics_fn_unconstrained, x, u)
                xs = out[0]
                us = out[1]
                cost = out[2]
                
            us = self.action_max * jnp.tanh(us) #self.action_max * jnp.tanh(us)
                
            out = [xs, us, cost]

        xs = out[0]
        us = out[1]
        cost = out[2]
        return xs, us, cost

# num_steps = 100
# initial_state = data['test']['states'][0] #jnp.array([jnp.pi / 2, 0.0])
# initial_state = jnp.array([0.0, 0.0, initial_state[2], 0.0])
# initial_actions = jnp.zeros(shape=(num_steps, u_dim)) #jnp.zeros(shape=(int(T / dt), u_dim))

# class ModelSelection():
#     '''
#         Class for model selection
#     '''
#     def __init__(self):
#         self.df = pd.read_csv("/home/honam/workspace/ode/pyur5/metadata/model_catalog.csv")
#         print(self.df.head())
#     def load_models(self, train_horizon=1, n_model=5):
#         # choose the column "train_horizon" == train_horizon
#         self.df_filter = self.df[self.df['train_horizon'] == train_horizon]        
        
#         # rank by the column "test_loss"
#         self.df_filter = self.df_filter.sort_values(by=['test_loss'])
        
#         # choose the top n_model
#         self.df_filter = self.df_filter.iloc[:n_model]
#         # get Name column
#         self.model_names = self.df_filter['Name'].values
        
#         print(self.model_names)
        
#         # load the models
#         models = []
#         params = []
#         for model_name in self.model_names:
#             # model_path = "../checkpoints/" + model_name + "/best_model.pkl"
#             # param_path = "../checkpoints/" + model_name + "/best_params.pkl"
#             # with open(model_path, "rb") as f:
#             #     model = pickle.load(f)
            
#             # with open(param_path, "rb") as f:
#             #     param = pickle.load(f)
            
#             model, param = self._load_model(model_name)
#             models.append(model)
#             params.append(param)
#         # self.models, self.params = jax.vmap(self._load_model)(self.model_names)
#         return models, params

#     @staticmethod
#     def _load_model(model_name):
#         model_path = "/home/honam/workspace/ode/pyur5/checkpoints/" + model_name + "/best_model.pkl"
#         param_path = "/home/honam/workspace/ode/pyur5/checkpoints/" + model_name + "/best_params.pkl"
#         with open(model_path, "rb") as f:
#             model = pickle.load(f)
        
#         with open(param_path, "rb") as f:
#             param = pickle.load(f)
            
#         return model, param
    
    

# catalog = ModelSelection()
# model = EnsembleModel(train_horizon=2, n_model=5)
