import sys
# sys.path.append('../mbse')
sys.path.append('/home/honam/workspace/ode/pyur5/include/pyur5')
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

from utils import denormalize, normalize, get_data_jax
import jax.numpy as jnp 
from jax.lax import cond
from trajax.optimizers import CEMHyperparams, ILQRHyperparams, ilqr_with_cem_warmstart, cem, ilqr
import pickle
import math
import jax
from models.models import MLP
from gym.spaces import Box
import numpy as np
from gym.envs.classic_control.pendulum import angle_normalize
import pandas as pd
from mbse.models.environment_models.pendulum_swing_up import PendulumReward, PendulumDynamicsModel, PendulumSwingUpEnv


class EnsembleModelSim():
    def __init__(self, env, train_horizon=1, n_horizon=50, cost_weights=jnp.array([10, 1, 1])):#, path_model, paths_params):
        self.train_horizon = train_horizon
        
        self.target_state = jnp.array([1,0])
        self.cost_weights = jnp.array([10, 1, 1])
        
        with open("/home/honam/workspace/ode/pyur5/model/best_model_sim_radiant-sweep-1.pkl", "rb") as f:
            self.model = pickle.load(f)

        
        self.cem_params = CEMHyperparams(max_iter=200, sampling_smoothing=0.0, num_samples=200, evolution_smoothing=0.0,
                        elite_portion=0.1)  
        # TODO: test with different values of psd_delta 
        self.ilqr_params = ILQRHyperparams(maxiter=200, make_psd=True, psd_delta=1e-2)

        with open("/home/honam/workspace/ode/pyur5/metadata/metadata_sim.pkl", "rb") as f:
            self.metadata = pickle.load(f)
        
        self.x_dim = 3
        self.u_dim = 1
        self.n_horizon = n_horizon
        
        self.obs_space = env.observation_space
        self.action_space = env.action_space
        
        self.pendulum_reward = PendulumReward(env.action_space)
        self.action_min = np.array([self.action_space.low[0]])
        self.action_max = np.array([self.action_space.high[0]])
        print("action_min", self.action_min)
        print("action_max", self.action_max)

        self.env = env
        self.true_dynamics = PendulumDynamicsModel(self.env)

    @staticmethod
    @jax.jit
    def normalize(x, u, metadata):
        inputs = jnp.concatenate([x, u], axis=-1)
        inputs = normalize(inputs, metadata['mu_x'], metadata['std_x'])
        return inputs
    
    @staticmethod
    @jax.jit
    def denormalize(out, metadata):    
        out = denormalize(out, metadata['mu_y'], metadata['std_y'])
        return out
    
    def _predict(self, xu):
        y_pred = self.model.predict(xu)
        mu, sig = jnp.split(y_pred, 2, axis=-1)
        ds = mu.mean(0)
        ds = self.denormalize(ds, self.metadata)
        return mu.mean(0)
    
    def normalize_and_predict(self, x, u):
        '''
            predict the dx given x and u after normalizing x and u
        '''
        print("x before normalize: ", x)
        print("u before normalize: ", u)
        xu = self.normalize(x, u, self.metadata)
        print("xu after normalize: ", xu)
        dx = self._predict(xu) #self.predict(xu)
        dx = self.denormalize(dx, self.metadata)
        return dx
            
    def step(self, x, u, sim_dynamics=False):
        '''
            predict the next state given x and u
            
            input:
                x: (x_dim, )
                u: (u_dim, ) rescaled action
            
            output:
                x_next: (x_dim, )
                terminate: bool
                truncate: bool
                output_dict: dict
        '''
        # print("x before step: ", x)
        # print("u before step: ", u)
        # u = u * 0.7 # scale to the range of [-0.7, 0.7]
        if sim_dynamics:
            print("sim_dynamics is True")
            x_next = self.true_dynamics.predict(x, u)
        else:
            dx = self.normalize_and_predict(x, u)
            x_next = x + dx
        return x_next, False, False, {}
    
    def _cost_fn(self, state, action, t): 
        print("_cost_fn called!")
        print("shape of state: ", state.shape)
        print("shape of action: ", action.shape)
        
        assert state.shape == (self.x_dim,) and action.shape == (self.u_dim,)

        reward = self.pendulum_reward.predict(state, action)
 

        return -reward

    def _dynamics_fn(self, x, u, t):
        '''
            Constrained dynamics function
        '''
        print(x.shape)
        print(u.shape)
        print("x of dynamics_fn: ", x)
        print("u of dynamics_fn: ", u)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        # u = self.action_max * jnp.tanh(u) # hypertangent trick 
        # TODO: Make this flexible so that we pass in values for sim_dynamics
        x_next, _, _, _ = self.step(x, u, sim_dynamics=False)
        print("x_next of dynamics_fn: ", x_next)
        return x_next
    
    def _dynamics_fn_unconstrained(self, x, u, t):
        print(x.shape)
        print(u.shape)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        u = self.action_max * jnp.tanh(u) # hypertangent trick 
        x_next, _, _, _ = self.step(x, u, sim_dynamics=False)
        print("x_next of dynamics_fn: ", x_next)
        return x_next

    def _dynamics_fn_cond(self, constrained=True):
        if constrained:
            return self._dynamics_fn
        else:
            return self._dynamics_fn_unconstrained
    
    def forward_traj(self, x, n_horizon, optimizer='cem_ilqr', sac_policy=None):
        # x_traj = jnp.zeros((n_horizon, x.shape[0]))
        print("x of forward_traj: ", x)
        self.n_horizon = n_horizon
        rng = jax.random.PRNGKey(seed=0)
        actor_rng, rng = jax.random.split(rng, 2)
        
        u = jnp.zeros((n_horizon, self.u_dim))

        # print(x.shape)
        print("u.shape", u.shape)

        if optimizer == 'cem':
            out = cem(self._cost_fn, self._dynamics_fn, x, u, control_low=self.action_min,
                              control_high=self.action_max, hyperparams=self.cem_params)
        elif optimizer == 'sac':
            if sac_policy is None:
                with open("/home/honam/workspace/ode/pyur5/metadata/sac_policy_{}_{}_{}.pkl".format(self.train_horizon, self.n_model, self.n_horizon), "rb") as f:
                    sac_policy = pickle.load(f)

            #f = jax.vmap(sac_policy.get_action_for_eval, (None, 0, 0))
            us = []
            xs = []
            cost = 0
            for i in range(self.n_horizon):
                u = sac_policy.get_action_for_eval(x, rng, 0)
                u = sac_policy.dynamics_model_list[0].rescale_action(u) # scale actions
                us.append(u)
                xs.append(x)
                x, _, _, _ = self.step(x, u)
                
                cost_single = self._cost_fn(x, u, i)
                
                #_, cost_single = sac_policy.dynamics_model_list[0].evaluate(xs[-1], u, rescaled=True)
                cost += cost_single
            out = [jnp.array(xs), jnp.array(us), cost]
        else:
            if optimizer == 'cem_ilqr':
                out = ilqr_with_cem_warmstart(self._cost_fn, self._dynamics_fn_unconstrained, x, u, control_low=self.action_min,
                                control_high=self.action_max, ilqr_hyperparams=self.ilqr_params, cem_hyperparams=self.cem_params)
            
            elif optimizer == 'ilqr':
                out = ilqr(self._cost_fn, self._dynamics_fn_unconstrained, x, u)
            
            xs = out[0]
            us = out[1]
            cost = out[2]
            
            us = self.action_max*jnp.tanh(us)
            out = [xs, us, cost]

        xs = out[0]
        us = out[1]
        cost = out[2]
        return xs, us, cost
