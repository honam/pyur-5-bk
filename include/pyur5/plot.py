import sys
sys.path.append('../mbse')
import time
from utils import denormalize, normalize, get_data_jax
import jax.numpy as jnp 
import matplotlib.pyplot as plt
from jax.lax import cond
from models.ens_model import EnsembleModel
import pickle
import jax
import wandb 
from gym.envs.classic_control.pendulum import angle_normalize
from utils import create_data

dt = 1/60
data_types = ['train', 'valid', 'test', 'train_old']
fpaths = ["../../data/recording_robot_{}.txt".format(i) for i in data_types]
data = dict()
moving_win = 8

dt = 1/60 # dt

n_horizons = [200, 300, 400, 500, 1000]#[20, 50, 100, 200]#[5, 10, 20, 30, 40, 50, 100, 150, 200]
train_horizons = [1]#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
n_models = [5]#[1, 5, 10]#[1, 5, 6, 7, 8, 9, 10]
solvers = ['cem_ilqr', 'cem', 'ilqr']
j_inits = [0]#[0, 100]

# n_horizon = 10 # n_horizon
# train_horizon = 1
# pred_horizon = 1
# n_model = 5

########## 1. Load data ##########
# xs = dict()
# ys = dict()
# us = dict()
# for train_horizon in train_horizons:
#     with open("../../data_pkl/data_{}.pkl".format(train_horizon), "rb") as f:
#         data = pickle.load(f)
#     data = data['test']
#     x = data['x'][:,:-1]
#     u = data['u']
#     y = data['y']

    # xs[train_horizon] = x
    # us[train_horizon] = u
    # ys[train_horizon] = y

########## 2. Load model ##########
# model = EnsembleModel(train_horizon=train_horizon, n_models=n_model)

########## 3. Predict ##########

# x_next, _, _, _ = jax.vmap(model.step)(x, u)

# with open("../../metadata/")
# rng = jax.random.PRNGKey(seed=0)
# actor_rng, rng = jax.random.split(rng, 2)
# action = jax.vmap(policy.get_action, (None, 0, 0))(x, rng, 0)#(obs=x, rng=rng, agent_idx=0)
# print("x[0] is {}".format(x[0]))
# print("u[0] is {}".format(u[0]))
# xs, us = model.forward_traj(x[0], 10, "sac")
# print("xs of shape {} is {}".format(xs.shape, xs))
# print("us of shape {} is {}".format(us.shape, us))
# print("x of shape {} is {}".format(x.shape, x))
# print("action of shape {} is {}".format(action.shape, action))

########## 4. Plot ##########
# test_horizon = 200
# x_test = x[:test_horizon]
# u_test = u[:test_horizon]
# y_test = y[:test_horizon]


headers = ["theta", "theta_dot", "p_ee", "v_ee"]

sweep_config = {
    'method': 'grid',
    "name": "ur5_test_plot_sweep",
    "parameters": {
        "train_horizon": {
            "values": train_horizons
        },
        "n_model": {
            "values": n_models
        },
        "n_horizon": {
            "values": n_horizons
        },
        "solver": {
            "values": solvers
        },
        "j_init": {
            "values": j_inits
        },
        "use_cos": {
            "values": [True]
        },
        "action_max": {
            "values": [1]
        }
    }
}
                    
def make_plot(config):
    train_horizon = config.train_horizon
    n_model = config.n_model
    n_horizon = config.n_horizon
    solver = config.solver
    j_init = config.j_init
    use_cos = config.use_cos
    action_max = config.action_max

    if action_max == 1:
        action_max = 1.0

    with open("../../data_pkl/data_{}_{}_{}.pkl".format(action_max,train_horizon, use_cos), "rb") as f:
        data = pickle.load(f)
    data = data['test']
    x = data['x'][:,:-1]
    u = data['u']
    y = data['y']
    
    model = EnsembleModel(use_cos=use_cos, train_horizon=train_horizon, n_model=n_model, n_horizon=n_horizon)

    # jax.vmap(model.forward_traj, in_axes=(None, 0, 0 ))(x[j_init], n_horizon, solvers)
    x_test, u_test, cost = model.forward_traj(x[j_init], n_horizon, solver)

    config = {"train_horizon": train_horizon, "n_model": n_model, "n_horizon": n_horizon, "solver": solver, "cost": cost, "j_init": j_init}
    
    # check if config has been run before with wandb
    # config_id = wandb.util.generate_config_id(config)

    # run = wandb.init(project="ur5_test_plot", config=config)
            
    xs = x_test.tolist()
    # theta = [x[0] for x in xs]
    # theta = [angle_normalize(x) for x in theta]
    # theta_dot = [x[1] for x in xs]
    # p_ee = [x[2] for x in xs]
    # v_ee = [x[3] for x in xs]
    
    # datas = [theta, theta_dot, p_ee, v_ee]
    # fig, axs = plt.subplots(5, 1, figsize=(20, 20))
    # fig.suptitle("{}_{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver, j_init))
    # for i in range(x_test.shape[1]):
    #     axs[i].plot(datas[i])
    #     axs[i].set_title(headers[i])
    # axs[-1].plot(u_test)
    # axs[-1].set_title("u")
    import math
    if use_cos:
        cos = [x[0] for x in xs]
        sin = [x[1] for x in xs]
        theta  = [math.atan2(sin[i], cos[i]) for i in range(len(cos))]
        theta_dot = [x[2] for x in xs]
        p_ee = [x[3] for x in xs]
        v_ee = [x[4] for x in xs]
        datas = [cos, sin, theta, theta_dot, p_ee, v_ee]
        headers = ["cos", "sin", "theta", "theta_dot", "p_ee", "v_ee"]

    else:
        theta = [x[0] for x in xs]
        theta = [angle_normalize(x) for x in theta]
        theta_dot = [x[1] for x in xs]
        p_ee = [x[2] for x in xs]
        v_ee = [x[3] for x in xs]

        datas = [theta, theta_dot, p_ee, v_ee]
        headers = ["theta", "theta_dot", "p_ee", "v_ee"]

    fig, axs = plt.subplots(len(headers) + 1, 1, figsize=(20, 20))
    fig.suptitle("{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver))
    for i in range(len(headers)):
        axs[i].plot(datas[i])
        axs[i].set_title(headers[i])
    axs[-1].plot(u_test)
    axs[-1].set_title("u")

    log = {"x_test": x_test, "u_test": u_test, "cost": cost, "plot": wandb.Image(fig)}
    fig.savefig("../../figs/{}_{}_{}_{}_{}.png".format(train_horizon, n_model, n_horizon, solver, j_init))
    # wandb.log(log)
    # wandb.finish()
    # fig.clf()
    with open("../../figs_log/{}_{}_{}_{}_{}.pkl".format(train_horizon, n_model, n_horizon, solver, j_init), "wb") as f:
        pickle.dump(log, f)
        
    fpath = "../../data/recording_robot_test.txt"

    create_data(theta, theta_dot, p_ee, v_ee, fpath, 'test', "{}_{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver, j_init), "test")
    return log

def main():
    wandb.init(project="ur5_test_plot")
    log = make_plot(wandb.config)
    wandb.log(log)

sweep_id = wandb.sweep(sweep_config, project="ur5_test_plot")

print("sweep_id is {}".format(sweep_id))
wandb.agent(sweep_id, function=main, count=100)