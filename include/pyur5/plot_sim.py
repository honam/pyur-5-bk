import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

import time
from utils import denormalize, normalize, get_data_jax
import jax.numpy as jnp 
import matplotlib.pyplot as plt
from jax.lax import cond
from models.ens_model_sim import EnsembleModelSim
import pickle
import jax
import wandb 
from gym.envs.classic_control.pendulum import angle_normalize
from utils import create_data
# from mbse.utils.vec_env.env_util import make_vec_env
from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv
from gym.wrappers import RescaleAction, TimeLimit

dt = 1/60
data_types = ['train', 'valid', 'test', 'train_old']
fpaths = ["../../data/recording_robot_{}.txt".format(i) for i in data_types]
data = dict()
moving_win = 8

dt = 1/60 # dt

n_horizons = [200, 300, 400, 500, 1000]#[20, 50, 100, 200]#[5, 10, 20, 30, 40, 50, 100, 150, 200]
train_horizons = [1]#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
n_models = [5]#[1, 5, 10]#[1, 5, 6, 7, 8, 9, 10]
solvers = ['cem_ilqr', 'cem', 'ilqr']
j_inits = [0]#[0, 100]

headers = [ 'cos', 'sin', 'theta_dot']
sweep_config = {
    'method': 'grid',
    "name": "ur5_test_plot_sweep",
    "parameters": {
        "train_horizon": {
            "values": train_horizons
        },
        "n_model": {
            "values": n_models
        },
        "n_horizon": {
            "values": n_horizons
        },
        "solver": {
            "values": solvers
        }
    }
}

def make_plot(config):
    train_horizon = config.train_horizon
    n_model = config.n_model
    n_horizon = config.n_horizon
    solver = config.solver

    env = CustomPendulumEnv()
    model = EnsembleModelSim(env)
    print("model loaded")
    x, _ = env.reset() # get a random initiial state
    print("env reset")
    print("x of shape {} is : {}".format(x.shape, x))

    # jax.vmap(model.forward_traj, in_axes=(None, 0, 0 ))(x[j_init], n_horizon, solvers)
    x_test, u_test, cost = model.forward_traj(x, n_horizon, solver)

    config = {"train_horizon": train_horizon, "n_model": n_model, "n_horizon": n_horizon, "solver": solver, "cost": cost}
    
    # check if config has been run before with wandb
    # config_id = wandb.util.generate_config_id(config)

    # run = wandb.init(project="ur5_test_plot", config=config)
            
    xs = x_test.tolist()
    cos = [x[0] for x in xs]
    sin = [x[1] for x in xs]
    theta_dot = [x[2] for x in xs]
    
    datas = [cos, sin, theta_dot]
    fig, axs = plt.subplots(4, 1, figsize=(20, 20))
    fig.suptitle("{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver))
    for i in range(x_test.shape[1]):
        axs[i].plot(datas[i])
        axs[i].set_title(headers[i])
    axs[-1].plot(u_test)
    axs[-1].set_title("u")
    
    log = {"x_test": x_test, "u_test": u_test, "cost": cost, "plot": wandb.Image(fig)}
    fig.savefig("../../figs/{}_{}_{}_{}.png".format(train_horizon, n_model, n_horizon, solver))
    # wandb.log(log)
    # wandb.finish()
    # fig.clf()
    # with open("../../figs_log/{}_{}_{}_{}_{}.pkl".format(train_horizon, n_model, n_horizon, solver, j_init), "wb") as f:
    #     pickle.dump(log, f)
        
    # fpath = "../../data/recording_robot_test.txt"

    # create_data(theta, theta_dot, p_ee, v_ee, fpath, 'test', "{}_{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver, j_init), "test")
    return log

def main():
    wandb.init(project="ur5_test_plot_sim_true")
    log = make_plot(wandb.config)
    wandb.log(log)

sweep_id = wandb.sweep(sweep_config, project="ur5_test_plot_sim_true")

print("sweep_id is {}".format(sweep_id))
wandb.agent(sweep_id, function=main, count=100)