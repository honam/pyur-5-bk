from utils import get_data_jax, normalize, denormalize, make_dataset, create_data#, loss_fn
import jax.numpy as jnp
import pickle
import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

import numpy as np
from mbse.utils.replay_buffer import ReplayBuffer, Transition
from models.ens_model import EnsembleModel
import jax
import os

def get_angles(states):
    return np.arctan2(states[:, 1], states[:, 0])


data_types = ['train', 'valid', 'test', 'train_old']
# data_types = ['train_old', 'valid', 'test']
action_max = 0.5

data = dict()
moving_win = 8
train_horizon = 1 
use_coss = [True]
for action_max in [1.0]:
    fpaths = ["../../data/recording_robot_{}_{}.txt".format(action_max, i) for i in data_types]
    for use_cos in use_coss:
        model = EnsembleModel(use_cos=use_cos, action_max=action_max)
        for i, data_type in enumerate(data_types[:-1]):
            if not os.path.exists("../../data_pkl/data_{}_{}_{}.pkl".format(action_max, train_horizon, use_cos)):
                states, x, y, u, ts, x_nexts = get_data_jax(fpaths[i], moving_win, train_horizon, use_cos=use_cos)
                if data_type == 'train':
                    if os.path.exists(fpaths[-1]):
                        states_old, x_old, y_old, u_old, ts_old, x_nexts_old = get_data_jax(fpaths[-1], moving_win, train_horizon, use_cos=use_cos)
                        states = jnp.append(states ,states_old, axis=0)
                        x = jnp.append(x, x_old, axis=0)
                        y = jnp.append(y, y_old, axis=0)
                        u = jnp.append(u, u_old, axis=0)
                        ts = jnp.append(ts, ts_old)
                        x_nexts = jnp.append(x_nexts, x_nexts_old, axis=0)
                data[data_type] = dict()
                data[data_type]['states'] = states
                data[data_type]['x'] = x
                data[data_type]['y'] = y
                data[data_type]['u'] = u
                data[data_type]['t'] = ts
                data[data_type]['x_nexts'] = x_nexts
                
                data[data_type]['mu_x'] = x.mean(0)
                data[data_type]['mu_y'] = y.mean(0)
                data[data_type]['std_x'] = x.std(0)
                data[data_type]['std_y'] = y.std(0)
            
            else:
                with open("../../data_pkl/data_{}_{}_{}.pkl".format(action_max, train_horizon, use_cos), "rb") as f:
                    data = pickle.load(f)
            states = data[data_type]['states']
            u = data[data_type]['u']
            x_nexts = data[data_type]['x_nexts']
            x = data[data_type]['x']
            
            print("data_type: {}, mu_x: {}, mu_y: {}, std_x: {}, std_y: {}".format(data_type, data[data_type]['mu_x'], data[data_type]['mu_y'], data[data_type]['std_x'], data[data_type]['std_y']))
            reward_vec = np.zeros((len(u), 1))
            done_vec = jnp.zeros((len(u), 1))
            # done_vec = np.abs(get_angles(x_nexts)) > 3.0
            
            cost_fn = jax.vmap(model._cost_fn, in_axes=(0, 0, 0))
            print("x: ", x.shape)
            print("u: ", u.shape)
            print("states: ", states.shape)
            reward_vec = cost_fn(states, u, jnp.arange(len(u)))
            reward_vec = -reward_vec.reshape(-1, 1)
            print("reward_vec: ", reward_vec.shape)
            
            data[data_type]['transitions'] = Transition(
                obs=states,
                action=u,
                reward=reward_vec,
                next_obs=x_nexts,
                done=done_vec,
            )

        # for i, data_type in enumerate(data_types[1:]):
        #     data[data_type]['x_norm'] = normalize(data[data_type]['x'], data['train']['mu_x'], data['train']['std_x'])
        #     data[data_type]['y_norm'] = normalize(data[data_type]['y'], data['train']['mu_y'], data['train']['std_y'])

        print(data)

        with open("../../data_pkl/data_{}_{}_{}.pkl".format(action_max, train_horizon, use_cos), "wb") as f:
            pickle.dump(data, f)


# num_data_pointss = [50000, 20000, 20000]
# n_rollout = 5000
# n_horizon_per_rollout = 5

# # model = EnsembleModel()
# data = dict()
# for i, data_type in enumerate(['train_old']):
#     num_data_points = num_data_pointss[i]
#     # n_rollout = 50
#     obs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))
#     action_vec = np.zeros((num_data_points, model.action_space.shape[0]))
#     next_obs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))
#     reward_vec = np.zeros((num_data_points, 1))
#     done_vec = np.zeros((num_data_points, 1))
#     dobs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))

#     n_rollout = int(num_data_points/n_horizon_per_rollout)
#     ind = 0

#     for i in range(n_rollout):
#         obs = model.obs_space.sample()
#         for j in range(n_horizon_per_rollout):
#             action = model.action_space.sample()
#             next_obs, reward, terminate, truncate = model.step(obs, action)
#             obs_vec[ind] = obs
#             action_vec[ind] = action
#             reward_vec[ind] = reward
#             next_obs_vec[ind] = next_obs
#             dobs_vec[ind] = next_obs - obs
#             done_vec[ind] = False #j == n_horizon_per_rollout - 1                
#             ind += 1
#             obs = next_obs

#     data[data_type] = dict()
#     data[data_type]['states'] = obs_vec
#     data[data_type]['x'] = jnp.concatenate([obs_vec, action_vec], axis=1)
#     data[data_type]['y'] = dobs_vec
#     data[data_type]['u'] = action_vec
        
#     data[data_type]['mu_x'] = data[data_type]['x'].mean(0)
#     data[data_type]['mu_y'] = dobs_vec.mean(0)
#     data[data_type]['std_x'] = data[data_type]['x'].std(0)
#     data[data_type]['std_y'] = dobs_vec.std(0)
    
#     data[data_type]['transitions'] = Transition(
#         obs=obs_vec,
#         action=action_vec,
#         reward=reward_vec,
#         next_obs=next_obs_vec,
#         done=done_vec,
#     )
    

# with open("../../data_pkl/model_data.pkl", "wb") as f:
#     pickle.dump(data, f)
