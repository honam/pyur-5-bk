from utils import get_data_jax, normalize, denormalize, make_dataset, create_data#, loss_fn
import jax.numpy as jnp
import pickle
import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

import numpy as np
from mbse.utils.replay_buffer import ReplayBuffer, Transition
from models.ens_model import EnsembleModel
import jax
import os

def get_angles(states):
    return np.arctan2(states[:, 1], states[:, 0])


# data_types = ['train_old', 'valid', 'test']
action_max = 1.0

data = dict()
moving_win = 8
train_horizon = 1 
use_coss = [True]#, False]
fpath = "../../data/recording_robot_pole.txt"
task_typ = 'pole'
for use_cos in use_coss:
    model = EnsembleModel(use_cos=use_cos, action_max=action_max, task_typ=task_typ)
    states, x, y, u, ts, states_next = get_data_jax(fpath, moving_win, train_horizon, use_cos=use_cos)

    data= dict()
    data['states'] = states
    data['x'] = x
    data['y'] = y
    data['u'] = u
    data['t'] = ts
    data['states_next'] = states_next
    
    data['mu_x'] = x.mean(0)
    data['mu_y'] = y.mean(0)
    data['std_x'] = x.std(0)
    data['std_y'] = y.std(0)

    print("mu_x: {}, mu_y: {}, std_x: {}, std_y: {}".format(data['mu_x'], data['mu_y'], data['std_x'], data['std_y']))
    reward_vec = np.zeros((len(u), 1))
    # done_vec = jnp.zeros((len(u), 1))
    done_vec = np.abs(get_angles(states_next)) < 2.5 # if angles drop below 2.5, it is considered as done 
    
    cost_fn = jax.vmap(model._cost_fn, in_axes=(0, 0, 0))
    print("x: ", x.shape)
    print("u: ", u.shape)
    print("states: ", states.shape)
    reward_vec = cost_fn(states, u, jnp.arange(len(u)))
    reward_vec = -reward_vec.reshape(-1, 1)
    print("reward_vec: ", reward_vec.shape)
    
    data['transitions'] = Transition(
        obs=states,
        action=u,
        reward=reward_vec,
        next_obs=states_next,
        done=done_vec,
    )

# for i, data_type in enumerate(data_types[1:]):
#     data[data_type]['x_norm'] = normalize(data[data_type]['x'], data['train']['mu_x'], data['train']['std_x'])
#     data[data_type]['y_norm'] = normalize(data[data_type]['y'], data['train']['mu_y'], data['train']['std_y'])

    print(data)

    with open("../../data_pkl/data_pole_{}_{}_{}.pkl".format(action_max, train_horizon, use_cos), "wb") as f:
        pickle.dump(data, f)


# num_data_pointss = [50000, 20000, 20000]
# n_rollout = 5000
# n_horizon_per_rollout = 5

# # model = EnsembleModel()
# data = dict()
# for i, data_type in enumerate(['train_old']):
#     num_data_points = num_data_pointss[i]
#     # n_rollout = 50
#     obs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))
#     action_vec = np.zeros((num_data_points, model.action_space.shape[0]))
#     next_obs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))
#     reward_vec = np.zeros((num_data_points, 1))
#     done_vec = np.zeros((num_data_points, 1))
#     dobs_vec = np.zeros((num_data_points, model.obs_space.shape[0]))

#     n_rollout = int(num_data_points/n_horizon_per_rollout)
#     ind = 0

#     for i in range(n_rollout):
#         obs = model.obs_space.sample()
#         for j in range(n_horizon_per_rollout):
#             action = model.action_space.sample()
#             next_obs, reward, terminate, truncate = model.step(obs, action)
#             obs_vec[ind] = obs
#             action_vec[ind] = action
#             reward_vec[ind] = reward
#             next_obs_vec[ind] = next_obs
#             dobs_vec[ind] = next_obs - obs
#             done_vec[ind] = False #j == n_horizon_per_rollout - 1                
#             ind += 1
#             obs = next_obs

#     data[data_type] = dict()
#     data[data_type]['states'] = obs_vec
#     data[data_type]['x'] = jnp.concatenate([obs_vec, action_vec], axis=1)
#     data[data_type]['y'] = dobs_vec
#     data[data_type]['u'] = action_vec
        
#     data[data_type]['mu_x'] = data[data_type]['x'].mean(0)
#     data[data_type]['mu_y'] = dobs_vec.mean(0)
#     data[data_type]['std_x'] = data[data_type]['x'].std(0)
#     data[data_type]['std_y'] = dobs_vec.std(0)
    
#     data[data_type]['transitions'] = Transition(
#         obs=obs_vec,
#         action=action_vec,
#         reward=reward_vec,
#         next_obs=next_obs_vec,
#         done=done_vec,
#     )
    

# with open("../../data_pkl/model_data.pkl", "wb") as f:
#     pickle.dump(data, f)
