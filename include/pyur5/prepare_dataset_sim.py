from utils import get_data_jax, normalize
import jax.numpy as jnp
import pickle

import numpy as np
import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

import pickle
import jax.numpy as jnp
from mbse.utils.vec_env.env_util import make_vec_env
from gym.wrappers import RescaleAction, TimeLimit
from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv
from mbse.utils.replay_buffer import ReplayBuffer, Transition
from models.ens_model_sim import EnsembleModelSim
data_types = ['train', 'valid', 'test']
data = dict()
moving_win = 8
train_horizon = 1 


time_limit = 5
n_envs = 1
seed = 0 
env_kwargs = {}

env = CustomPendulumEnv()#, wrapper_class=wrapper_cls)#, n_envs=n_envs, seed=seed, env_kwargs=env_kwargs)


num_data_pointss = [50000, 20000, 20000]

for i, data_type in enumerate(data_types):
    num_data_points = num_data_pointss[i]
    n_rollout = 50
    obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
    action_vec = np.zeros((num_data_points, env.action_space.shape[0]))
    next_obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
    reward_vec = np.zeros((num_data_points, 1))
    done_vec = np.zeros((num_data_points, 1))
    dobs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))

    sample_per_rollout = int(num_data_points/n_rollout)
    ind = 0
    for i in range(n_rollout):
        obs, _ = env.reset_random()
        for j in range(sample_per_rollout):
            action = env.action_space.sample()
            next_obs, reward, done, info, _ = env.step(action)
            obs_vec[ind] = obs
            action_vec[ind] = action
            reward_vec[ind] = reward
            next_obs_vec[ind] = next_obs
            dobs_vec[ind] = next_obs - obs
            done_vec[ind] = done
            obs = next_obs
            
            ind += 1

    data[data_type] = dict()
    data[data_type]['states'] = obs_vec
    data[data_type]['x'] = jnp.concatenate([obs_vec, action_vec], axis=1)
    data[data_type]['y'] = dobs_vec
    data[data_type]['u'] = action_vec
        
    data[data_type]['mu_x'] = data[data_type]['x'].mean(0)
    data[data_type]['mu_y'] = dobs_vec.mean(0)
    data[data_type]['std_x'] = data[data_type]['x'].std(0)
    data[data_type]['std_y'] = dobs_vec.std(0)
    
    data[data_type]['transitions'] = Transition(
        obs=obs_vec,
        action=action_vec,
        reward=reward_vec,
        next_obs=next_obs_vec,
        done=done_vec,
    )
    

with open("../../data_pkl/sim_data.pkl", "wb") as f:
    pickle.dump(data, f)


model = EnsembleModelSim(env)
data = dict()
for i, data_type in enumerate(data_types):
    num_data_points = num_data_pointss[i]
    n_rollout = 50
    obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
    action_vec = np.zeros((num_data_points, env.action_space.shape[0]))
    next_obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
    reward_vec = np.zeros((num_data_points, 1))
    done_vec = np.zeros((num_data_points, 1))
    dobs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))

    sample_per_rollout = int(num_data_points/n_rollout)
    ind = 0
    for i in range(n_rollout):
        obs, _ = env.reset_random()
        xs, us, cost = model.forward_traj(obs, sample_per_rollout, 'cem')
        for j in range(sample_per_rollout):
            obs_vec[ind] = xs[j]
            action_vec[ind] = us[j]
            reward_vec[ind] = model._cost_fn(xs[j], us[j], j)
            next_obs_vec[ind] = xs[j+1]
            dobs_vec[ind] = xs[j+1] - xs[j]
            done_vec[ind] = j == sample_per_rollout - 1                
            ind += 1

    data[data_type] = dict()
    data[data_type]['states'] = obs_vec
    data[data_type]['x'] = jnp.concatenate([obs_vec, action_vec], axis=1)
    data[data_type]['y'] = dobs_vec
    data[data_type]['u'] = action_vec
        
    data[data_type]['mu_x'] = data[data_type]['x'].mean(0)
    data[data_type]['mu_y'] = dobs_vec.mean(0)
    data[data_type]['std_x'] = data[data_type]['x'].std(0)
    data[data_type]['std_y'] = dobs_vec.std(0)
    
    data[data_type]['transitions'] = Transition(
        obs=obs_vec,
        action=action_vec,
        reward=reward_vec,
        next_obs=next_obs_vec,
        done=done_vec,
    )
    

with open("../../data_pkl/sim_model_data.pkl", "wb") as f:
    pickle.dump(data, f)
