# make sbatch file for preprocess.py for each data
import os
import multiprocessing
# use pool to run multiple sbatch files at once
import time
import sys
import subprocess

for i in range(10):
    with open("slurm_agent.sh", "r") as f:
        lines = f.readlines()
    # lines.insert(6, "#SBATCH --output=../../slurm_log/{}.out\n".format(i))
    os.system("sbatch slurm_agent.sh ")