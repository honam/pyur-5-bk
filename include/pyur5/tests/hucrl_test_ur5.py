import jax
import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')
sys.path.append('/home/honam/workspace/ode/pyur5/include/pyur5')

from mbse.agents.model_based.model_based_agent import ModelBasedAgent
from mbse.models.active_learning_model import ActiveLearningHUCRLModel
from mbse.utils.replay_buffer import ReplayBuffer
from gym.wrappers.rescale_action import RescaleAction
from gym.wrappers.time_limit import TimeLimit
from mbse.utils.vec_env.env_util import make_vec_env
from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv, PendulumReward
import pickle 
import matplotlib.pyplot as plt
from models.ens_model import EnsembleModel
from gym.envs.classic_control.pendulum import angle_normalize
from mbse.models.ur5_dynamics_model import Ur5PendulumReward
import jax.numpy as jnp
import math
# wrapper_cls = lambda x: RescaleAction(
#         TimeLimit(x, max_episode_steps=200),
#         min_action=-1,
#         max_action=1,
#     )

# env = make_vec_env(env_id=CustomPendulumEnv, wrapper_class=wrapper_cls, n_envs=1, seed=0)
train_horizon = 1
n_model = 10
n_horizon = 500
solver = 'ilqr'
use_cos = True
action_max = 1.0

model = EnsembleModel(use_cos=use_cos, action_max=action_max)
lr = 5e-4
# reward_model = Ur5PendulumReward(
#             action_space = model.action_space,
#             min_action = model.action_min,
#             max_action = model.action_max,
#             target_state = model.target_state,
#             cost_weights = model.cost_weights
#         )
reward_model = Ur5PendulumReward(model)
#reward_model.set_bounds(max_action=model.action_max)

print("model.action_space: ", model.action_space)
print("model.obs_space: ", model.obs_space)

dynamics_model = ActiveLearningHUCRLModel(
            action_space=model.action_space,
            observation_space=model.obs_space,
            num_ensemble=5,
            reward_model=reward_model,
            features=[256, 256],
            pred_diff=True,
            beta=1.0,
            seed=0,
            use_log_uncertainties=True,
            use_al_uncertainties=False,
            deterministic=True,
            lr=lr,
)

print("dynamics_model created")

agent = ModelBasedAgent(
        train_steps=200000,
        batch_size=256,
        max_train_steps=8000,
        num_epochs=50,
        action_space=model.action_space,
        observation_space=model.obs_space,
        dynamics_model=dynamics_model,
        n_particles=10,
        reset_model=True,
        policy_optimizer_name='TraJaxTO',
        horizon=20,
)

print("agent created")
print("model.obs_space.shape: ", model.obs_space.shape)
print("model.action_space.shape: ", model.action_space.shape)
buffer = ReplayBuffer(
    obs_shape=model.obs_space.shape,
    action_shape=model.action_space.shape,
    normalize=True,
    action_normalize=True,
    learn_deltas=True,
)

print("buffer created")

with open("../../../data_pkl/data_{}_{}_{}.pkl".format(action_max,train_horizon, use_cos), "rb") as f:
    data = pickle.load(f)

sim_transitions = data['train']['transitions']
print("sim_transitions: ", sim_transitions)
buffer.add(transition=sim_transitions) #add unnormalized data

print("buffer added")

agent.set_transforms(
    bias_obs=buffer.state_normalizer.mean,
    bias_act=buffer.action_normalizer.mean,
    bias_out=buffer.next_state_normalizer.mean,
    scale_obs=buffer.state_normalizer.std,
    scale_act=buffer.action_normalizer.std,
    scale_out=buffer.next_state_normalizer.std,
)

print("agent set transforms")

rng = jax.random.PRNGKey(0)
rng, train_rng = jax.random.split(rng, 2)
# obs, _ = env.reset(seed=0)

print("agent train step")
total_train_steps = agent.train_step(
    rng=train_rng,
    buffer=buffer,
    validate=True,
    log_results=False, # can change if you want to log to wandb
)

# test_env = make_vec_env(env_id=CustomPendulumEnv, wrapper_class=wrapper_cls, n_envs=1, seed=0)
#                         #env_kwargs={'render_mode': 'human'})
# obs, _ = test_env.reset(seed=0)
if use_cos:
    obs = jnp.array([1.0, 0.0, 0.0, 0.0, 0.0])
else:
    obs = jnp.array([0.0, 0.0, 0.0, 0.0])
    
done = False
steps = 0
idx = 0

obss = []
next_obss = []
actions = []
while not done:

    action = agent.act(obs, rng=rng, eval=True, eval_idx=0)
    next_obs, reward, terminate, truncate = model.step(obs, action)
    action = model.action_max * action
    reward = reward_model.predict(obs, action)
    done = steps >= n_horizon
    steps += 1
    print("Step: {}, Reward: {}, obs: {}, next_obs: {}, action: {}".format(steps, reward, obs, next_obs, action))
    with open("data.csv", "a") as f:
        f.write("{},{},{},{}, {}\n".format(steps, reward, obs, next_obs, action))
    # print(steps)
    obss.append(obs)
    next_obss.append(next_obs)
    actions.append(action * model.action_max)
    
    obs = next_obs

    # if done:
    #     obs, _ = env.reset(seed=0)
if use_cos:
    cos = [x[0] for x in obss]
    sin = [x[1] for x in obss]
    theta  = [math.atan2(sin[i], cos[i]) for i in range(len(cos))]
    theta_dot = [x[2] for x in obss]
    p_ee = [x[3] for x in obss]
    v_ee = [x[4] for x in obss]
    datas = [cos, sin, theta, theta_dot, p_ee, v_ee]
    headers = ["cos", "sin", "theta", "theta_dot", "p_ee", "v_ee"]

else:
    theta = [x[0] for x in obss]
    theta = [angle_normalize(x) for x in theta]
    theta_dot = [x[1] for x in obss]
    p_ee = [x[2] for x in obss]
    v_ee = [x[3] for x in obss]

    datas = [theta, theta_dot, p_ee, v_ee]
    headers = ["theta", "theta_dot", "p_ee", "v_ee"]

fig, axs = plt.subplots(len(headers) + 1, 1, figsize=(20, 20))
fig.suptitle("{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver))
for i in range(len(headers)):
    axs[i].plot(datas[i])
    axs[i].set_title(headers[i])
axs[-1].plot(actions)
axs[-1].set_title("u")

fig.savefig("real_model.png")