#import jax.numpy as jnp
#from jax import grad, jit, vmap
import json
import numpy as np
import torch
import jax.numpy as jnp
from jax import vmap
import tensorflow as tf
import tensorflow_datasets as tfds
import trajax

#def load_model(model_path):
#    model = jnp.load(model_path)
#    return model

# read txt json file
def read_json(file_path):
    with open(file_path) as f:
        data = json.load(f)
    return data

def states2ind_states(states):
    '''
        Extract useful states from the states
        
        states: (N, 24)
    '''
    thetas = states[:,12]
    theta_dots = states[:,13]
    p_pivots = states[:,14:17]
    v_ees = states[:,17:20]
    p_balls = states[:,20:23]
    mocap_valids = states[:,23]
    states = states[:,0:12]
    return states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids

def pytorch_rolling_window(x, window_size, step_size=1):
    # unfold dimension to make our rolling window
    return x.unfold(0,window_size,step_size)

def normalize(x, mu_x, std_x, eps=1e-8):
    return (x - mu_x)/(std_x + eps)

def denormalize(x, mu_x, std_x, eps=1e-8):
    return (std_x + eps)*x + mu_x

def create_data(theta, theta_dot, p_pivot, v_ee, fpath, data_type, j, test=None, pred_horizon=1):
    '''
        Create data in json for running in the simulation
    '''
    with open(fpath, 'r') as f:
        file = f.readlines()
    modified_json_lines = []
    i = 0
    for k, line in enumerate(file):
        # Parse the line into a JSON object
        json_obj = json.loads(line)
        if test is None:
            if json_obj[1]['state'][-1][0] < 0.5:
                print("{}".format(i))
            else:
                #print(i)
                # Modify the object (for example, add a new key-value pair)
                #json_obj[0]['new_key'] = 'new_value'
                # theta
                json_obj[1]['state'][12][0] = theta[i]
                # theta_dot
                json_obj[1]['state'][13][0] = theta_dot[i]
                # p_pivot
                #json_obj[1]['state'][14:17] = [[x] for x in p_pivot[i]]
                json_obj[1]['state'][14][0] =  p_pivot[i]
                # v_ee
                #json_obj[1]['state'][17:20] = [[x] for x in v_ee[i]]
                json_obj[1]['state'][17][0] = v_ee[i]

                # Convert the modified object back into a JSON-formatted string
                modified_json_line = json.dumps(json_obj) + '\n'
                modified_json_lines.append(modified_json_line)
                i += 1
        else:
            json_obj[1]['state'][12][0] = theta[i]
            # theta_dot
            json_obj[1]['state'][13][0] = theta_dot[i]
            # p_pivot
            #json_obj[1]['state'][14:17] = [[x] for x in p_pivot[i]]
            json_obj[1]['state'][14][0] =  p_pivot[i]
            # v_ee
            #json_obj[1]['state'][17:20] = [[x] for x in v_ee[i]]
            json_obj[1]['state'][17][0] = v_ee[i]

            # Convert the modified object back into a JSON-formatted string
            modified_json_line = json.dumps(json_obj) + '\n'
            modified_json_lines.append(modified_json_line)
            i+=1 
            if i == len(theta):
                break
    # Write the modified JSON lines to a new file
    with open("../../data/recording_robot_{}_{}.txt".format(data_type, j), 'w') as f:
        f.writelines(modified_json_lines)

def moving_average(x, window_size):
    """Compute the moving average of a sequence using a sliding window."""
    x = jnp.concatenate((jnp.array([0.0]*(window_size-1)),x))
    weights = jnp.repeat(1.0, window_size) / window_size
    return jnp.convolve(x, weights, mode='valid')

# def get_data_jax(fpath, moving_win=None, train_horizon=1, use_cos=False):
    '''
        retrieve data from json file
    '''
    ts = []
    states = []
    actions = []
    with open(fpath, "r") as f:
        for line in f.readlines():
            data =json.loads(line)
            ts.append(data[0]["t"])
            states.append(data[1]['state'])
            actions.append(data[2]['input'])

    ts = jnp.array(ts)
    states = jnp.array(states)
    actions = jnp.array(actions)

    #states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    states = states[:,:,0]
    actions = actions[:,0,0]
    print(states.shape)
    print(actions.shape)
    states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    #states = [thetas.reshape(thetas.shape[0],-1), theta_dots.reshape(theta_dots.shape[0],-1), p_pivots[:,0], v_ees[:,0]]
    states = [thetas, theta_dots, p_pivots[:,0], v_ees[:,0]]

    # reshape tensors
    states = jnp.concatenate([jnp.expand_dims(state,-1) for state in states],1)
    actions = jnp.expand_dims(actions,-1)
    states_chunk = jnp.array([])
    ts = jnp.expand_dims(ts, -1)
    inds = [0] + jnp.where(mocap_valids < 1.0)[0].tolist() + [len(mocap_valids)]
    idxes = [(inds[i], inds[i+1]) for i in range(len(inds)-1)]
    # TODO: Check if this makes big difference in performance
    xs = jnp.array([])
    ys = jnp.array([])
    us = jnp.array([])


    for i, idx in enumerate(idxes):
        state_chunk = states[idx[0]:idx[1]]
        action_chunk = actions[idx[0]:idx[1]]
        ts_chunk = ts[idx[0]:idx[1]]

        if idx[1] - idx[0] < train_horizon:
            # skip if the chunk is too small
            continue 

        if moving_win is not None:
            # perform moving average on the chunk
            state_chunk = state_chunk.at[:,1].set(moving_average(state_chunk[:,1], moving_win))
            state_chunk = state_chunk.at[:,3].set(moving_average(state_chunk[:,3], moving_win))
            #state_chunk = moving_average(state_chunk, moving_win)
            
            action_chunk = action_chunk.at[:,0].set(moving_average(action_chunk[:,0], moving_win))

        # modify this to take consideration of the horizon
        if use_cos:
            cos_theta = jnp.cos(state_chunk[:,0]).reshape(-1,1)
            sin_theta = jnp.sin(state_chunk[:,0]).reshape(-1,1)
            # print("cos_theta.shape: {}".format(cos_theta.shape))
            # print("sin_theta.shape: {}".format(sin_theta.shape))
            state_chunk = jnp.concatenate([cos_theta, sin_theta, state_chunk[:,1:]], 1)
        x = state_chunk[:-train_horizon]
        x_next = state_chunk[train_horizon:]

        y = x_next - x

        if not use_cos:
            y = y.at[:,0].set(jnp.arctan2(jnp.sin(y[:,0]), jnp.cos(y[:,0]))) # normalize the angle difference
        u = action_chunk[:-train_horizon]
        # print("action_chunk.shape: {}".format(action_chunk.shape))
        # print("x.shape: {}".format(x.shape))
        # print("y.shape: {}".format(y.shape))
        # print("u.shape: {}".format(u.shape))

        x = jnp.concatenate([x, u], 1)

        # x = state_chunk[:-1]
        # y = state_chunk[1:] - state_chunk[:-1]
        # y = y.at[:,0].set(jnp.arctan2(jnp.sin(y[:,0]), jnp.cos(y[:,0]))) # normalize the angle difference
        # u = action_chunk[:-1]
        # x = jnp.concatenate([x, u], 1)

        # print("action_chunk.shape: {}".format(action_chunk.shape))

        if i == 0:
            states_chunk = state_chunk[:-train_horizon]
            #action_chunk = action_chunk
            #tss_chunk
            xs = x
            ys = y
            us = u
            x_nexts = x_next
        else:
            states_chunk = jnp.append(states_chunk, state_chunk[:-train_horizon], axis=0)
            #actions_chunk = jnp.append(actions_chunk, action_chunk, axis=0)
            #ts_chunk = jnp.append(ts_chunk, ts_chunk, axis=0)

            xs = jnp.append(xs, x, axis=0)
            ys = jnp.append(ys, y, axis=0)
            us = jnp.append(us, u, axis=0)
            x_nexts = jnp.append(x_nexts, x_next, axis=0)
        # x = states[:-1]
        # y = states[1:] - states[:-1]
        # u = actions[:-1]
        # x = jnp.concatenate([x, u], 1) 
        # u = actions
    states = states_chunk
    # print((mocap_valids>0).sum())
    print("states.shape: ", states.shape)
    print("xs.shape: ", xs.shape)
    print("ys.shape: ", ys.shape)
    print("us.shape: ", us.shape)
    print("x_nexts.shape: ", x_nexts.shape)
    return states, xs, ys, us, ts, x_nexts

def get_data_jax(fpath, moving_win=None, train_horizon=1, use_cos=False):
    '''
        retrieve data from json file
    '''
    ts = []
    states = []
    actions = []
    with open(fpath, "r") as f:
        for line in f.readlines():
            data =json.loads(line)
            ts.append(data[0]["t"])
            states.append(data[1]['state'])
            actions.append(data[2]['input'])
    ts = np.array(ts)
    states = np.array(states)
    actions = np.array(actions)
    states = states[:,:,0]
    actions = actions[:,0,0]

    states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    mocap_valids = np.logical_and(mocap_valids[train_horizon:] == True, mocap_valids[:-train_horizon] == True)
    mocap_valids = np.concatenate([mocap_valids, np.array([False])])
    
    idx1 = actions != 0.0
    idx_now = np.logical_and(idx1, mocap_valids)

    idx_next = np.roll(idx_now, train_horizon)
    for i in range(train_horizon):
        idx_next[i] = False
    
    print(idx_now)
    if use_cos:
        states_total = [np.cos(thetas), np.sin(thetas), theta_dots, p_pivots[:,0], v_ees[:,0]]
    else:
        states_total = [thetas, theta_dots, p_pivots[:,0], v_ees[:,0]]

    # reshape tensors
    states_total = np.concatenate([jnp.expand_dims(state,-1) for state in states_total], 1)
    actions = np.expand_dims(actions,-1)

    states = states_total[idx_now]
    us = actions[idx_now]
    ys = states_total[idx_next] - states_total[idx_now]
    if not use_cos:
        ys[:,0] = np.arctan2(np.sin(ys[:,0]), np.cos(ys[:,0]))
    xs = np.concatenate([states, us], -1)
    states_next = states_total[idx_next]
    # states_nexts = np.concatenate([states_total[idx_next], actions[idx_next]],-1)
    # print((mocap_valids>0).sum())
    print("xs.shape: ", xs.shape)
    print("ys.shape: ", ys.shape)
    print("us.shape: ", us.shape)
    # print("x_nexts.shape: ", x_nexts.shape)
    return states, xs, ys, us, ts, states_next

def get_data(fpath, n_horizon=1):
    ts = []
    states = []
    actions = []
    with open(fpath, "r") as f:
        for line in f.readlines():
            data =json.loads(line)
            ts.append(data[0]["t"])
            states.append(data[1]['state'])
            actions.append(data[2]['input'])

    ts = np.array(ts)
    states = np.array(states)
    actions = np.array(actions)


    #states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    states = states.squeeze(-1)
    actions = actions[:,0,0]
    states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    #states = [thetas.reshape(thetas.shape[0],-1), theta_dots.reshape(theta_dots.shape[0],-1), p_pivots[:,0], v_ees[:,0]]
    states = [thetas, theta_dots, p_pivots[:,0], v_ees[:,0]]
    states = [torch.from_numpy(state).unsqueeze(-1) for state in states]
    states = torch.concat(states,1)
    actions= torch.from_numpy(actions).unsqueeze(-1)
    ts = torch.from_numpy(ts).unsqueeze(-1)
    
    states = states[mocap_valids>0]
    actions = actions[mocap_valids>0]
    ts = ts[mocap_valids>0]
    x = states[:-1]
    y = states[1:] - states[:-1]
    u = actions[:-1]
    x = torch.cat([x,u], 1)
    u = actions
    print((mocap_valids>0).sum())
    return states, x, y, u, ts

def make_dataset(data, data_types, batch_size):
    shuffles = [True, False, False]
    # infinite = True

    datasets = []
    for i , data_type in enumerate(data_types):
        #xs = data[data_type]['x']
        #xs_norm = normalize(xs, data['train']['mu_x'], data['train']['std_x'])
        #ys = data[data_type]['y']
        #ys_norm = normalize(ys, data['train']['mu_y'], data['train']['std_y'])
        
        xs_norm = data[data_type]['x_norm']
        ys_norm = data[data_type]['y_norm']
        
        ds = tf.data.Dataset.from_tensor_slices((xs_norm, ys_norm))
        if shuffles[i]:
            seed = 0
            ds = ds.shuffle(buffer_size=4 * batch_size, seed=seed, reshuffle_each_iteration=True)
        # if infinite:
        #     ds = ds.repeat()
        ds = ds.batch(batch_size)
        ds = tfds.as_numpy(ds)
        datasets.append(ds)
    return datasets


def dynamics_fn(state, action, model, mu_y, std_y):
    xu = jnp.concatenate([state, action], 1)
    dx = model(xu)
    dx = denormalize(dx, mu_y, std_y)
    return state + dx

def cost_fn(state, action, qs, target_state):
    theta = state[0]
    theta_dot = state[1]
    # p_pivot = state[2]
    # v_ee = state[3]
    
    q_theta = qs[0]
    q_theta_dot = qs[1]
    q_u = qs[2]
    
    theta_star = target_state[0]
    theta_dot_star = target_state[1]
    
    L = q_theta * (theta - theta_star)**2 + \
        q_theta_dot * (theta_dot - theta_dot_star)**2 + \
        q_u * action**2 
    
    return L

    
def objective(cost, dynamics, initial_state, actions, qs, target_state):
    state = initial_state
    n_horizon = actions.shape[0]
    L = 0.0
    for t in range(n_horizon):
        loss = cost(state, actions[t], qs, target_state)
        state = dynamics(state, actions[t])
        L += loss
    return L    

# def improve_controls(cost, dynamics, U, x0, eta, n_iters):
#     grad_fn = jax.grad(trajax.objective, argnums=(4,))
#     for i in range(n_iters):
#         U = U - eta * grad_fn(cost, dynamics, x0, U)
#     return U 

