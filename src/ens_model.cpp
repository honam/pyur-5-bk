#include <pyur5/ens_model.h>
#include <string>
namespace py = pybind11;

ur5CartPole::FCModel::FCModel(int _train_horizon, int _n_model, int _n_horizon) {
    calc_ = py::module_::import("ens_model");
    train_horizon = _train_horizon;
    n_model = _n_model;
    n_horizon = _n_horizon;
    model_ = calc_.attr("EnsembleModel")(train_horizon, n_model, n_horizon);
}

ur5CartPole::FCModel::~FCModel() {
    model_.release();
    calc_.release();
}

// Eigen::VectorXd ur5CartPole::FCModel::predict(const Eigen::VectorXd &obs) {
//     py::object action_py = model_.attr("predict")(obs);
//     return action_py.cast<Eigen::VectorXd>();
// }

// Eigen::VectorXd ur5CartPole::FCModel::step(const Eigen::VectorXd &obs, const Eigen::VectorXd &u) {
//     py::object action_py = model_.attr("predict")(obs);
//     return action_py.cast<Eigen::VectorXd, bool, bool, py::dict>();
// }

std::tuple<Eigen::VectorXd, Eigen::VectorXd, double> ur5CartPole::FCModel::forward_traj(const Eigen::VectorXd &obs, const int &n_steps, const std::string &optimizer) {
    py::object action_py = model_.attr("forward_traj")(obs, n_steps, optimizer);
    return action_py.cast<std::tuple<Eigen::VectorXd, Eigen::VectorXd, double>>();
}

